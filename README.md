# File Uploading Techniques
File uploading can me an important step, if a pentest requires the use of scripts
on a compromised device. 

Below are a few ways, to make a file transfer. 

# Python server 
python -m SimpleHTTPServer 8000
```ruby
    - wget - linux
    - IWR (invoke web request) Windows
```

2. SFTP over SSH login
Simply using sftp username@ipaddress will connect in with SFTP capabilities, and 
allow for cli file uploads/downloads.